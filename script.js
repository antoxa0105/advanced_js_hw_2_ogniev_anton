// try/catch позволяет предотвратить блокирование выполнения кода при ошибке в самом коде или при некорректных
// входящих данных

const root = document.getElementById('root');
const list = document.createElement('ul');

const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  }
]; 

books.forEach((el) => {
  try {
    if (el.author && el.name && el.price) {
      const listElement = document.createElement('li');
      listElement.innerHTML = `${el.author}<br>${el.name}<br>${el.price}`;
      list.append(listElement)
    } else {
      const error = `error:${!el.author ? 'no autor property' : !el.name ? 'no name property' : 'no price property'}`
      throw error;
    };
  } catch (err) {
    console.log(err);
  }
})

root.append(list);
